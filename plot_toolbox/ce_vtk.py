import numpy as np
import vtk


def actor_to_polyData(actor):
    polyData = vtk.vtkPolyData()
    polyData.DeepCopy(actor.GetMapper().GetInput())
    transform = vtk.vtkTransform()
    transform.SetMatrix(actor.GetMatrix())
    fil = vtk.vtkTransformPolyDataFilter()
    fil.SetTransform(transform)
    fil.SetInputDataObject(polyData)
    fil.Update()
    polyData.DeepCopy(fil.GetOutput())
    return polyData


def polyData_to_actor(polydata, alpha=1, color=(155 / 255, 155 / 255, 155 / 255)):
    """Wrap the provided vtkPolyData object in a mapper and an actor, returning
    the actor."""
    mapper = vtk.vtkPolyDataMapper()
    if vtk.VTK_MAJOR_VERSION <= 5:
        # mapper.SetInput(reader.GetOutput())
        mapper.SetInput(polydata)
    else:
        # mapper.SetInputConnection(polydata.GetProducerPort())
        mapper.SetInputData(polydata)
    actor = vtk.vtkActor()
    actor.SetMapper(mapper)
    # actor.GetProperty().SetRepresentationToWireframe()
    actor.GetProperty().SetColor(color)
    actor.GetProperty().SetOpacity(alpha)
    return actor


def stl_reader_out(filename, return_mapper=True, return_reader=False):
    reader = vtk.vtkSTLReader()
    reader.SetFileName(filename)
    reader.Update()
    if return_reader:
        return reader
    
    if return_mapper:
        mapper = vtk.vtkPolyDataMapper()
        mapper.SetInputConnection(reader.GetOutputPort())
        return mapper


def create_actor(mapper, alpha=1, color=(155 / 255, 155 / 255, 155 / 255)):

    actor = vtk.vtkActor()
    actor.SetMapper(mapper)
    actor.GetProperty().SetColor(color)
    actor.GetProperty().SetOpacity(alpha)

    return actor


def create_points_node(p3d=np.zeros((2, 3))):
    # Create the geometry of a point (the coordinate)
    points = vtk.vtkPoints()

    # Create the topology of the point (a vertex)
    vertices = vtk.vtkCellArray()
    for p in p3d:
        ii = points.InsertNextPoint(p)
        vertices.InsertNextCell(1)
        vertices.InsertCellPoint(ii)

    # Create a polydata object
    point = vtk.vtkPolyData()

    # Set the points and vertices we created as the geometry and topology of the polydata
    point.SetPoints(points)
    point.SetVerts(vertices)

    mapper = vtk.vtkPolyDataMapper()
    mapper.SetInputData(point)
    
    return mapper


def create_renders(back_ground_color=(10 / 255, 10 / 255, 10 / 255), *arg, **kwargs):
    # Create a rendering window and renderer
    ren = vtk.vtkRenderer()
    ren.SetBackground(back_ground_color)
    renWin = vtk.vtkRenderWindow()
    renWin.AddRenderer(ren)
    return ren, renWin


def create_interactor(renWin):
    iren = vtk.vtkRenderWindowInteractor()
    iren.SetRenderWindow(renWin)
    style = vtk.vtkInteractorStyleTrackballCamera()
    iren.SetInteractorStyle(style)
    return iren

def get_screenshot(renWin, filename="./screenshot.tif"):
    w2if = vtk.vtkWindowToImageFilter()
    w2if.SetInput(renWin)
    w2if.Update()

    ext = filename.split(".")[-1]
    
    if ext == "tif":
        writer = vtk.vtkTIFFWriter()
        writer.SetFileName(filename)
        writer.SetCompressionToNoCompression()
    elif ext == "png":
        writer = vtk.vtkPNGWriter()
        writer.SetFileName(filename)
    elif ext == "jpg":
        writer = vtk.vtkJPEGWriter()
        writer.SetFileName(filename)
    else:
        raise ValueError(f'Wrong file extension, only tif, png and jpg are supported \r\n Specified extension is "{ext}"')
    
 
    writer.SetInputData(w2if.GetOutput())
    writer.Write()


from vtkmodules.vtkInteractionStyle import vtkInteractorStyleTrackballCamera

class VtkRender():
    def __init__(self, *args, **kwargs) -> None:

        self.ren, self.renWin = create_renders(*args, **kwargs)
        self.iren = create_interactor(self.renWin)
        
        self.iren.AddObserver("MiddleButtonPressEvent",
                              self.middle_button_press_event)
        
        self.actors = {}
        self.actor_count = 0
        self.dir_screenshot = "./"
        self.screenshot_name = "screenshot.tif"
        
        if "dir_screenshot" in kwargs:
            self.dir_screenshot = kwargs["dir_screenshot"]
        if "screenshot_name" in kwargs:
            self.screenshot_name = kwargs["screenshot_name"]
            

    def load_stl_actor(self, stl_filename: str, actor_name: str = None, *args, **kwargs):

        if actor_name is None:
            actor_name = f"actor_{self.actor_count}"
            self.actor_count += 1

        actor = create_actor(stl_reader_out(
            filename=stl_filename), *args, **kwargs)
        
        self.actors.update({actor_name: actor})
        
        return actor
    
    def load_actor(self, actor, actor_name: str = None):
        if actor_name is None:
            actor_name = f"actor_{self.actor_count}"
            self.actor_count += 1
        self.actors.update({actor_name: actor})

    
    def add_legend(self, background = (0 / 255, 0 / 255, 0 / 255)):
        
        legend = vtk.vtkLegendBoxActor()
        legend.SetNumberOfEntries(2)
        legendbox = vtk.vtkCubeSource()
        legendbox.Update()
        for name, actor in self.actors.items():           
            legend.SetEntry(list(self.actors.keys()).index(name),
                            legendbox.GetOutput(), name, actor.GetProperty().GetColor())

        
        # // place legend in lower right
        # legend.GetPositionCoordinate().SetCoordinateSystemToView();
        # legend.GetPositionCoordinate().SetValue(0.5, -1.0);

        # legend.GetPosition2Coordinate().SetCoordinateSystemToView();
        # legend.GetPosition2Coordinate().SetValue(1.0, -0.5);

        # legend.UseBackgroundOn();
        legend.SetBackgroundColor(background);
        self.ren.AddActor(legend)
        
    
    def show_scene(self):
                      
        [self.ren.AddActor(actor) for name, actor in self.actors.items()]
        [print(f"Loaded actor: {name}")for name, actor in self.actors.items()]
        
        self.add_legend()
        
        self.iren.Initialize()
        self.renWin.Render()
        self.iren.Start()

    def get_screenshot(self):       
        get_screenshot(self.renWin, filename=self.dir_screenshot + self.screenshot_name)
        
    def middle_button_press_event(self, obj, event):
        self.get_screenshot()
        print('Screenshot taken')
        return
    
    def lef_button_press_event(self, obj, event):
        print('Press left click')
        return