import copy
import math
from math import pi, sin, cos, sqrt
from string import ascii_lowercase as abc_list

import cv2
import matplotlib.pyplot as plt
import numpy as np
from cv2 import aruco
from plot_toolbox import ce_plot
import itertools

aruco_dict = aruco.Dictionary_get(aruco.DICT_6X6_250)

################################################################################
# PROJECTIVE GEOMETRY


def invert_RT(R, T):
    """
    Inverts a RT transformation
    """
    Ti = -cv2.Rodrigues(-R)[0].dot(T)
    return -R, Ti


def compose_RT(R1, T1, R2, T2):
    """
    Composed 2 RT transformations
    """
    R, T = cv2.composeRT(R1, T1, R2, T2)[:2]
    return R.flatten(), T.flatten()


def change_reference(R10, T10, R20, T20):
    """
    Change the reference frame in which RT 2 transformation is expressed. The
    new reference frame is 1.
    Return R21,T21
    """
    R01, T01 = invert_RT(R10, T10)
    return compose_RT(R20, T20, R01, T01)


def apply_RT(P, R, T):
    """
    Applies RT transformation to 3D points P.
    """
    P = cv2.Rodrigues(R)[0].dot(P.T).T
    P += T.T
    return P


def get_camera_parameters(sensor_h, sensor_w, resolution_h, resolution_w, F):
    """_summary_

    Args:
        sensor_h (_type_): _description_
        sensor_w (_type_): _description_
        resolution_h (_type_): _description_
        resolution_w (_type_): _description_
        F (_type_): _description_

    Returns:
        _type_: _description_
    """
    Fy = Fx = F
    fy = Fy / (sensor_h/resolution_h)  # pixels
    fx = Fx / (sensor_w/resolution_w)  # pixels

    cx = resolution_w / 2
    cy = resolution_h / 2

    camera_matrix = np.array([[fx, 0, cx],
                              [0, fy, cy],
                              [0, 0, 1]], np.float32)
    dist_coeff = np.zeros(5)

    dist_coeff = np.zeros(5)
    return camera_matrix, dist_coeff


def resize_img(img, width, height):
    """_summary_

    Args:
        img (_type_): _description_
        width (_type_): _description_
        height (_type_): _description_

    Returns:
        _type_: _description_
    """
    dim = (width, height)
    return cv2.resize(img, dim, interpolation=cv2.INTER_AREA)


def oblique_projection(P, alpha=45.):
    alpha = math.radians(alpha)
    mat = np.array([[1., 0., .5 * math.cos(alpha)],
                    [0., 1., .5 * math.sin(alpha)],
                    ])
    return mat.dot(P.T).T


def get_cube(lx=1.0, ly=1.0, lz=1.0, **kwargs):
    _cvert = np.array(
        [
            [0.0, 0.0, 0.0],
            [1.0, 0.0, 0.0],
            [1.0, 1.0, 0.0],
            [0.0, 1.0, 0.0],
            [0.0, 0.0, 1.0],
            [1.0, 0.0, 1.0],
            [1.0, 1.0, 1.0],
            [0.0, 1.0, 1.0],
        ]
    )
    _cedges = np.array(
        [
            [0, 1],
            [1, 2],
            [2, 3],
            [3, 0],
            [4, 5],
            [5, 6],
            [6, 7],
            [7, 4],
            [0, 4],
            [1, 5],
            [2, 6],
            [3, 7],
        ]
    )
    _cfaces = np.array(
        [
            [0, 1, 2, 3],
            [4, 5, 6, 7],
            [0, 1, 5, 4],
            [1, 5, 6, 2],
            [2, 6, 7, 3],
            [3, 7, 4, 0],
            [3, 7, 4, 0],
        ]
    )
    verts = _cvert * np.array([lx, ly, lz])
    return Polyhedron(vertices=verts, edges=_cedges, faces=_cfaces, **kwargs)


def get_camera(lx=1.0, ly=1.0, lz=1.0, l=0.1, **kwargs):
    _cvert = np.array(
        [
            [0.0, 0.0, 0.0],            # 0
            [1.0, 0.0, 0.0],            # 1
            [1.0, 1.0, 0.0],            # 2
            [0.0, 1.0, 0.0],            # 3
            [0.0, 0.0, 1.0],            # 4
            [1.0, 0.0, 1.0],            # 5
            [1.0, 1.0, 1.0],            # 6
            [0.0, 1.0, 1.0],            # 7
            [0.0-l, 0.0-l, 1.0+l],      # 8
            [1.0+l, 0.0-l, 1.0+l],      # 9
            [1.0+l, 1.0+l, 1.0+l],      # 10
            [0.0-l, 1.0+l, 1.0+l],      # 11
        ]
    )
    _cedges = np.array(
        [
            [0, 1],  # square 0
            [1, 2],  # square 0
            [2, 3],  # square 0
            [3, 0],  # square 0
            [4, 5],  # square 1
            [5, 6],  # square 1
            [6, 7],  # square 1
            [7, 4],  # square 1
            [0, 4],  # square 2
            [1, 5],  # square 2
            [2, 6],  # square 3
            [3, 7],  # square 3
            [8, 9],  # square 4
            [9, 10],  # square 4
            [10, 11],  # square 4
            [11, 8],  # square 4
            [4, 8],  # quad 1-4
            [5, 9],  # quad 1-2
            [6, 10],  # quad 2-3
            [7, 11],  # quad 3-4
        ]
    )
    _cfaces = np.array(
        [
            [0, 1, 2, 3],
            [4, 5, 6, 7],
            [0, 1, 5, 4],
            [1, 5, 6, 2],
            [2, 6, 7, 3],
            [3, 7, 4, 0],
            [3, 7, 4, 0],
            [6, 7, 11, 10],
            [4, 5, 9, 8],
            [4, 7, 11, 8],
            [6, 5, 9, 10],
        ]
    )

    verts = _cvert * np.array([lx, ly, lz])
    return Polyhedron(vertices=verts, edges=_cedges, faces=_cfaces, **kwargs)


def get_cone(H=1, R=1, n=10, **kwargs):
    v = np.zeros(((n + 1), 3))
    # print(v.shape)

    v[0, 2] = H
    theta = np.linspace(0.0, 2.0 * pi, n, endpoint=False)
    for i in range(n):
        t = theta[i]
        v[i + 1, 0] = R * cos(theta[i])
        v[i + 1, 1] = R * sin(theta[i])
    edges = []
    for i in range(n):
        e = [0, i + 1]
        edges.append(e)
    for i in range(n - 1):
        e = [i + 1, i + 2]
        edges.append(e)
    edges.append([n - 1, 1])
    faces = []
    for i in range(n):
        f = [0, i, i + 1]
        faces.append(f)
    faces[-1][2] = 1
    faces.append(np.arange(n) + 1)
    return Polyhedron(vertices=v, edges=edges, faces=faces, **kwargs)


def get_plane(lx, ly, **kwargs):
    """
    Get a plane with the given length in x and y direction.
    """
    _cvert = np.array(
        [
            [-0.5, -0.5, 0.0],
            [0.5, -0.5, 0.0],
            [0.5, 0.5, 0.0],
            [-0.5, 0.5, 0.0],
        ]
    )

    _cedges = np.array(
        [
            [0, 1],
            [1, 2],
            [2, 3],
            [3, 0]
        ]
    )

    _cfaces = np.array(
        [
            [0, 1, 2, 3],
        ]
    )

    verts = _cvert * np.array([lx, ly, 0])
    return Polyhedron(vertices=verts, edges=_cedges, faces=_cfaces, **kwargs)


def get_line(P0=np.zeros(3), P1=np.array([1.0, 0, 0]), **kwargs):
    """
    Get a line with the given length.
    """
    _cvert = np.array(
        [
            P0,
            P1,
        ]
    )

    _cedges = np.array(
        [
            [0, 1],
        ]
    )

    _cfaces = np.array(
        [
            [0, 1],
        ]
    )

    verts = _cvert
    return Polyhedron(vertices=verts, edges=_cedges, faces=_cfaces, **kwargs)


def get_polyline(P, **kwargs):
    """
    Get a polyline with the given length.
    """
    _cvert = P

    _cedges = np.zeros([len(P)-1, 2]).astype(int)
    for i in range(len(P)-1):
        _cedges[i, 0] = i
        _cedges[i, 1] = i+1
    _cfaces = _cedges.copy()

    verts = _cvert

    return Polyhedron(vertices=verts, edges=_cedges, faces=_cfaces, **kwargs)


def get_point(P=np.zeros(3), **kwargs):
    """
    Get a point with the given position.
    """
    _cvert = np.array(
        [
            [P[0], P[1], P[2]],
        ]
    )

    verts = _cvert
    return Polyhedron(vertices=verts, edges=None, faces=None, **kwargs)


def get_tetrahedron(b=1, h=2, scale=1.0, **kwargs):
    """
    Get a tetrahedron with the given radius.
    """
    # h = r/3
    # r_p = sqrt(r**2 - h**2)
    # a = sqrt(r_p**2 + (r+h)**2)
    # b = sqrt((3*a**2) /4) - r_p

    # _cvert = np.array(
    #     [
    #         [r_p, 0, -h],
    #         [-b, a/2, -h],
    #         [-b, -a/2, -h],
    #         [0, 0, 1.],
    #     ]
    # )

    _cvert = np.array(
        [
            [(8/9)**0.5, 0, -1/3],
            [-(2/9)**0.5, (2/3)**0.5, -1/3],
            [-(2/9)**0.5, -(2/3)**0.5, -1/3],
            [0, 0, 1],
        ]
    )
    _cvert = apply_RT(_cvert, R=np.zeros(3), T=np.array([0, 0, -1]))

    _cedges = np.array(
        [
            [0, 1],
            [1, 2],
            [2, 0],
            [0, 3],
            [1, 3],
            [2, 3],
        ]
    )

    _cfaces = np.array(
        [
            [0, 1, 2],
            [0, 1, 3],
            [0, 2, 3],
            [1, 2, 3],
        ]
    )

    verts = (_cvert.T * (np.array([b, b, b, h])*scale)).T
    return Polyhedron(vertices=verts, edges=_cedges, faces=_cfaces, **kwargs)


def get_dodecaheron(b=1, h=2, scale=1.0, **kwargs):
    """
    Get a regular dodecaheron centered at origin.
    phi = (1 + 5**0.5)/2
    edget length = 2 / phi = 5**0.5 -1
    circumradius = 3**0.5
    """
    phi = (1 + 5**0.5)/2

    _cube_vertx = np.array(list(itertools.product([-1, 1], repeat=3)))
    
    _rect0_vertx = np.zeros([4,3])
    _rect0_vertx[:,1:] = np.array(list(itertools.product(
        *zip([-phi, -(1/phi)], [phi, (1/phi)]))))
    
    _rect1_vertx = np.zeros([4,3])
    _rect1_vertx[:,0] = np.array(list(itertools.product(
        *zip([-(1/phi), -phi], [(1/phi), phi]))))[:,0]
    _rect1_vertx[:,2] = np.array(list(itertools.product(
        *zip([-(1/phi), -phi], [(1/phi), phi]))))[:,1]
    
    _rect2_vertx = np.zeros([4,3])
    _rect2_vertx[:,:2] = np.array(list(itertools.product(
        *zip([-phi, -(1/phi)], [phi, (1/phi)]))))
    
    _cvert = np.concatenate(
        (_cube_vertx, _rect0_vertx, _rect1_vertx, _rect2_vertx), axis=0)
    
    _cvert = np.dot(np.eye(_cvert.shape[0])*scale, _cvert)
    
    _cedges = np.array(
        [
            [0, 8],
            [8, 9],
            [9, 1],
            [1, 16],
            [16, 0], # 0
            [9, 5],
            [5, 18],
            [18, 4],
            [4, 8],
            [4,14],
            [14,12],
            [12,0],
            [18,19],
            [19, 6],
            [6, 14], 
            [14, 12],
            [12,2], 
            [2, 10],
            [10, 6 ],
            [1 , 13],
            [13,3], 
            [3, 17],
            [17, 16],
            [13, 15],
            [15, 5],
            [15, 7],
            [7,19],
            [7,11],
            [11,3],
            [11,10],
            [17,2]
        ]
    )
    _cfaces = np.array(
        [
            [0, 16, 1, 9, 8],
            [0, 12, 14, 4, 8],
            [4, 14, 6, 19, 18],
            [12, 14, 6, 10, 2],
            [11, 7, 15, 13, 3],
            [3, 17, 2, 10, 11],
            [16, 0, 12, 2, 17],
            [9,8,4,18,5],
            [1,9,5,15,13],
            [16,1,13,3,17],
            [5,15,7,19,18],
            [7,11,10,6,19],
        ]
    )
    verts = _cvert
    return Polyhedron(vertices=verts, edges=_cedges, faces=_cfaces, **kwargs)


class Image:
    def __init__(self, nx=8, ny=8, scale=1) -> None:
        self.nx = nx
        self.ny = ny

        x = (np.linspace(0.0, 1.0, self.nx) +
             1 / self.nx / 2) / (1 + 1 / self.nx)
        y = (np.linspace(0.0, 1.0, self.ny) +
             1 / self.ny / 2) / (1 + 1 / self.ny)

        X, Y = np.meshgrid(x, y)
        self.X = (X.flatten() - 0.5) * scale
        self.Y = (Y.flatten() - 0.5) * scale

    def set_z_source(self, z):
        """_summary_set_img
          Set source of the image
        Args:
            source (array_like): source image

        Returns:
            _type_: _description_
        """
        if z.shape[0] != self.nx and z.shape[1] != self.ny:
            z = resize_img(z, self.nx, self.ny)
        self.z = z
        return self

    def get_p3d(self):
        """_summary_
          Get 3D points
        Returns:
            _type_: _description_
        """
        self.p3d = np.zeros((len(self.X), 3))
        self.p3d[:, 0] = self.X
        self.p3d[:, 1] = self.Y
        return self

    def apply_RT(self, R=np.zeros(3), T=np.zeros(3)):
        """_summary_
          Apply rotation and translation to the image 3 points from image reference frame to world reference frame
        Args:
            R (_type_, optional): _description_. Defaults to np.zeros(3).
            T (_type_, optional): _description_. Defaults to np.zeros(3).

        Returns:
            _type_: _description_
        """
        self.get_p3d()
        self.p3d = apply_RT(self.p3d, R=R, T=T)
        return self


class Polyhedron:
    def __init__(self, vertices=None, edges=None, faces=None, R=np.zeros(3), T=np.zeros(3), **kwargs):
        self.vertices = vertices
        self.edges = edges
        self.faces = faces

        self.reset_R = np.zeros(3)
        self.reset_T = np.zeros(3)

        self.R = np.zeros(3)
        self.T = np.zeros(3)

        self.apply_RT(R=R, T=T)

    def project(
        self,
        ax,
        projection,
        plot_vertices=True,
        vertices_color="r",
        plot_edges=True,
        edge_color="r",
        edge_alpha=1.0,
        plot_faces=True,
        face_color="g",
        face_alpha=1.0,
        vertice_alpha=1.0,
        debug=False,
        zorder=False,
        ls="-",
        marker="o",
        markersize=1.5,
        linewidth=1.,
    ):
        Verts = self.vertices
        edges = self.edges
        faces = self.faces
        verts = projection(Verts)
        self.projected_verts = verts
        x, y = verts.T

        map_zorder = {}
        _ = [map_zorder.update({v: i})
             for i, v in enumerate(np.unique(Verts[:, 2]))]
        order = [map_zorder[z] for z in Verts[:, 2]]

        if zorder:
            map_zorder_edge = {}
            _ = [map_zorder_edge.update({v/2: i}) for i, v in enumerate(
                np.unique([np.sum(Verts[:, 2][edge], axis=0) for edge in edges]))]
            order_edge = [map_zorder_edge[s/2]
                          for s in [np.sum(Verts[:, 2][edge], axis=0) for edge in edges]]
            map_zorder_face = {}
            _ = [map_zorder_face.update({v/4: i}) for i, v in enumerate(
                np.unique([np.sum(Verts[:, 2][face], axis=0) for face in faces]))]
            order_face = [map_zorder_face[s/4]
                          for s in [np.sum(Verts[:, 2][face], axis=0) for face in faces]]

            if plot_vertices and (Verts is not None):
                for i in range(len(Verts)):
                    ax.scatter(x[i], y[i], zorder=order[i],
                               linewidths=markersize,
                                alpha=vertice_alpha,
                               marker=marker, color=vertices_color)
                    if debug:
                        ax.text(x[i], y[i], str(i), color="k", fontsize=24)
            if plot_edges and (edges is not None):
                for i, edge in enumerate(edges):
                    ax.plot(x[edge], y[edge], zorder=order_edge[i],
                            ls=ls, color=edge_color, alpha=edge_alpha, linewidth=linewidth)
            if plot_faces and (faces is not None):
                for i, face in enumerate(faces):
                    ax.fill(x[face], y[face], face_color,
                            zorder=order_face[i], alpha=face_alpha)
        else:
            if plot_vertices and (Verts is not None):
                for i in range(len(Verts)):
                    ax.scatter(x[i], y[i],
                               linewidths=markersize, marker=marker, color=vertices_color)
                    if debug:
                        ax.text(x[i], y[i], str(i), color="k", fontsize=24)
            if plot_edges and (edges is not None):
                for edge in edges:
                    ax.plot(x[edge], y[edge], ls=ls,
                            color=edge_color, linewidth=linewidth)
            if plot_faces and (faces is not None):
                for face in faces:
                    ax.fill(x[face], y[face], face_color, alpha=face_alpha)

    def apply_RT(self, R=np.zeros(3), T=np.zeros(3)):

        fR, fT = compose_RT(R1=self.R, T1=self.T, R2=R, T2=T)
        self.reset_R, self.reset_T = invert_RT(R=fR, T=fT)

        self.R = R
        self.T = T
        self.vertices = apply_RT(self.vertices, self.R, self.T)
        return self

    def text(self, ax, projection, text="ref", offset_X=0, offset_Y=0,
             bbox_props=ce_plot.get_bbox_props(alpha=0, pad_size=0.1, lw=0.1), **kwargs):
        """
        kwargs : 
            va="center",
            ha="center",
            ma="left",
            fontsize=font_size,

        """
        position = self.T
        X, Y = projection(position)

        ax.text(
            X+offset_X,
            Y+offset_Y,
            text,
            bbox=bbox_props,
            **kwargs
        )

        return self


class ObliqueProjection:
    def __init__(self, alpha):
        self.alpha = alpha

    def project(self, P):
        alpha = math.radians(self.alpha)
        mat = np.array(
            [[1.0, 0.0, -0.5 * math.cos(alpha)],
             [0.0, 1.0, -0.5 * math.sin(alpha)], ]
        )
        return mat.dot(P.T).T


class PerspectiveProjection:
    def __init__(self, focal=1.0):
        self.focal = focal

    def project(self, P, model_type=None):
        """_summary_

        Args:
            P (ndarray): Points in 3D space to be projected, dimension (N,3)
            model_type (string, optional): If equal to "pinhole_camera", automatically apply 
                pi rotation along y-axis on P. Defaults to None.

        Returns:
            ndarray: projected points in 2D space, dimension (N,2)
        """

        if model_type == "pinhole_camera":
            P = apply_RT(P, R=np.array(
                [0.0, pi, 0.0]), T=np.zeros(3))
        focal = self.focal
        out = P[:, :2].copy()
        out *= focal / P[:, 2:]
        return out


class ReferenceFrame:
    def __init__(self, R=np.zeros(3), T=np.zeros(3), dim=3, size=1.0):
        self.size = size
        self.dim = dim
        self.R = R
        self.T = T
        self.apply_RT(R=R, T=T, initRT=True)

    def apply_RT(self, R=np.zeros(3), T=np.zeros(3), initRT=False):
        if not initRT:
            self.R_rev, self.T_rev = invert_RT(self.R, self.T)
            self.R, self.T = compose_RT(R1=self.R, T1=self.T, R2=R, T2=T)
        return self

    def project(self, edge_color=list("rgb"), tip_type="cone", *args, **kwargs):
        size = self.size
        rotations = np.zeros((3, 3))
        rotations[0, :] = np.array([0.0, pi / 2, 0.0])
        rotations[1, :] = np.array([-pi / 2, 0.0, 0.0])

        self.bodies = {"x": None, "y": None, "z": None}
        self.tips = {"x": None, "y": None, "z": None}
        str_axis = "xyz"
        for i in range(self.dim):
            v = np.zeros((2, 3))
            v[1, 2] = size
            edges = np.array([[0, 1]])
            self.body = Polyhedron(vertices=v, edges=edges)
            self.body = self.body.apply_RT(R=rotations[i])
            self.body = self.body.apply_RT(self.R, self.T)
            self.body.project(
                ax=kwargs["ax"],
                plot_edges=True,
                projection=kwargs["projection"],
                edge_color=edge_color[i],
                face_color=edge_color[i],
                plot_vertices=False,
            )
            self.bodies[str_axis[i]] = self.body
            
            if tip_type == "cone":
                self.tip = get_cone(R=0.05 * size, H=0.2 * size, n=100)
                self.tip.apply_RT(T=np.array([0.0, 0.0, size]))
                self.tip = self.tip.apply_RT(R=rotations[i])
                self.tip.apply_RT(self.R, self.T)
                self.tip.project(
                    edge_color=edge_color[i], face_color=edge_color[i], *args, **kwargs
                )
            else:
                self.tip = get_tetrahedron(scale=0.05 * size)
                self.tip.apply_RT(T=np.array([0.0, 0.0, size]))
                self.tip = self.tip.apply_RT(R=rotations[i])
                self.tip.apply_RT(self.R, self.T)
                self.tip.project(
                    edge_color=edge_color[i], face_color=edge_color[i], *args, **kwargs
                )
            self.tips[str_axis[i]] = self.tip

    def text(self, ax, projection, text="ref", offset_X=0, offset_Y=0,
             bbox_props=ce_plot.get_bbox_props(alpha=0, pad_size=0.1, lw=0.1), **kwargs):
        """
        kwargs : 
            va="center",
            ha="center",
            ma="left",
            fontsize=font_size,

        """
        position = self.T
        X, Y = projection(position)

        ax.text(
            X+offset_X,
            Y+offset_Y,
            text,
            bbox=bbox_props,
            **kwargs
        )

        return self


class Arrow:

    def __init__(self, P0, P1, R=np.zeros(3), T=np.zeros(3), size=1.0):
        self.R = R
        self.T = T
        self.P = np.array([P0, P1])
        self.size = size

    def apply_RT(self, R=np.zeros(3), T=np.zeros(3)):
        self.R, self.T = compose_RT(self.R, self.T, R, T)
        return self

    def project(self, tip_type="cone", size=1.0, *args, **kwargs):
        edges = np.array([[0, 1]])
        self.body = Polyhedron(vertices=self.P, edges=edges)
        self.body = self.body.apply_RT(self.R, self.T)
        self.body.project(
            plot_edges=True,
            **kwargs
        )
        if tip_type == "cone":
            kwargs.pop("ls", None)
            self.tip = get_cone(R=0.05 * size, H=0.3 * size, n=10)
            self.tip.apply_RT(T=np.array(
                [0.0, 0.0, np.linalg.norm(self.P)*1.001]))
            self.tip.apply_RT(self.R, self.T)
            self.tip.project(
                ls="solid",
                **kwargs
            )
        else:
            self.tip = get_tetrahedron(scale=0.05 * size)
            self.tip.apply_RT(T=np.array([0.0, 0.0,  np.linalg.norm(self.P)]))
            self.tip.apply_RT(self.R, self.T)
            self.tip.project(
                *args, **kwargs
            )


if __name__ == "__main__":

    # cube = get_cube()
    # # cone = get_cone(H=0.2, R = 0.1 )
    # # cone.apply_RT(R = np.array([0., pi/2, 0.]), T=np.array([1., 0., 0.]))
    # ref = ReferenceFrame(size=1.2)

    # R = np.array([pi / 10, pi / 15, 0.0])
    # T = np.zeros(3)
    # T[2] = 1.
    # Rcam = np.array([0.0, pi, 0.0])
    # R, T = compose_RT(R, T, Rcam, np.zeros(3))

    # n = 128
    # img = Image(nx=n, ny=n)
    # img.set_z_source(z=aruco.drawMarker(aruco_dict, 12, n)) \
    #    .apply_RT(R=R, T=T) \

    # P = img.p3d

    # cube.apply_RT(R, T)
    # ref = ref.apply_RT(R, T)

    # proj = ObliqueProjection(45.0).project
    # proj = PerspectiveProjection(focal=1.0).project

    # p = proj(P)
    # x = p[:, 0].reshape(n, n)
    # y = p[:, 1].reshape(n, n)
    # z = img.z.reshape(n, n)
    # fig, ax = plt.subplots()
    # # plt.gca().invert_yaxis()
    # ax.set_aspect("equal")
    # # plt.scatter(p[:, 0], p[:, 1], s = 4)
    # # plt.contourf(x, y,z, 30, cmap = "binary")
    # plt.pcolormesh(x, y, z, cmap="gray", shading="nearest")
    # cube.project(ax, proj, face_alpha=.1)
    # ref.project(
    #     ax=ax, projection=proj, face_alpha=1.0, plot_edges=True, plot_vertices=False
    # )

    ref_scene = ReferenceFrame(size=0.33, dim=3, T=np.array([0.0, 0.0, 0.0]))
    polyhedron = get_dodecaheron(scale=0.1)
    camera = get_camera()
    proj = ObliqueProjection(45.0).project

    # ref_scene.apply_RT(R= np.array( [-pi/2, 0 ,0]))
    # polyhedron.apply_RT(R= np.array([-pi/2, 0 ,0]))

    fig, ax = plt.subplots()
    ref_scene.project(ax=ax, projection=proj, tip_type="tetrahedron",
                      face_alpha=1.0, plot_edges=True, plot_vertices=False)

    polyhedron.project(ax, proj, face_alpha=0.1, debug=False,
                       plot_edges=True,
                       plot_faces=True)


    ax.set_aspect("equal")
    ax.axis("off")
    # plt.savefig("fig.png", dpi=200)
    
    from mpl_toolkits.mplot3d import Axes3D
    from mpl_toolkits.mplot3d.art3d import Poly3DCollection

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')

    tt = {}
    [tt.update({f"{i}":[polyhedron.vertices[i,:]]}) for i in range(polyhedron.vertices[:,0].shape[0])]
    # ax.scatter(polyhedron.vertices[:,0], polyhedron.vertices[:,1], polyhedron.vertices[:,2], "k")
    # for i in range(polyhedron.vertices[:,0].shape[0]):
        # ax.text(polyhedron.vertices[i,0], polyhedron.vertices[i,1], polyhedron.vertices[i,2], f"{i}", size=10, zorder=1, color='k', fontsize=16)
    
    # for verts in polyhedron.vertices:
    # ax.plot_trisurf(polyhedron.vertices[:9,0], polyhedron.vertices[:9,1], polyhedron.vertices[:9,2])
    # plt.show()
    
    def get_tri_from_face(face):
        return face[:3], face[2:], face[::2]

    for face in polyhedron.faces:      
        pnt = []
        for subface in get_tri_from_face(face):
            for ed in subface:
                pnt.append(polyhedron.vertices[ed])

        pnt = np.array(pnt)
        ax.add_collection3d(Poly3DCollection(pnt, alpha=0.5))
    
    x, y, z = polyhedron.vertices.T
    for i, edge in enumerate(polyhedron.edges):
        ax.plot(xs=x[edge], ys=y[edge],zs=z[edge],  ls="-", color="k", alpha=1, linewidth=0.5)
    
    ax.set_xlim3d(x.min(), x.max())
    ax.set_ylim3d(y.min(), y.max())
    ax.set_zlim3d(z.min(), z.max())
    ax.grid(False)
    ax.axis("off")
    plt.show()
    