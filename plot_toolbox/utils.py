import cv2
import numpy as np 

def Rodrigues_to_homogenous_matrix(R, T):
    # Given a Rodrigues rotation vector and a translation vector, return the homogeneous matrix
    R = cv2.Rodrigues(R)[0]
    T = np.array(T).reshape(3, 1)
    return np.concatenate(
        [np.concatenate([R, T], axis=1), np.array([[0, 0, 0, 1]])], axis=0
    )

def homogenous_matrix_to_Rodrigues(H):
    # Given a homogeneous matrix, return the Rodrigues rotation vector and the translation vector
    R = H[:3, :3]
    T = H[:3, 3]
    return cv2.Rodrigues(R)[0], T
