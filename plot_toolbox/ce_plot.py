from curses import COLOR_GREEN
import os

import cv2
from string import ascii_lowercase
from PIL import Image
import matplotlib as mpl
import numpy as np
from matplotlib import pyplot as plt
from matplotlib.patches import Arc
from matplotlib.transforms import Bbox, IdentityTransform, TransformedBbox
from mpl_toolkits.axes_grid1.inset_locator import mark_inset

import plot_toolbox
from plot_toolbox.rcparams import RCPARAMS

mpl.rcParams.update(RCPARAMS)
DEFAULT_IMG_PATH = f'{plot_toolbox.__path__[0]}/data/default-placeholder.png'
COLORS = {"orange": "#FF9E00", "green": "#00D453", "blue": "#00A8FF"}

def save_plot(plt, path, pdfcrop=True, compiler="lualatex", dpi=100, bbox_inches="tight", backend="pgf"):
    RCPARAMS["pgf.texsystem"] = compiler
    plt.savefig(path, dpi=dpi, bbox_inches=bbox_inches, backend=backend)
    if pdfcrop: 
        pdf_crop(filename=path)

def pdf_crop(filename: str):
    """_summary_

    Args:
        filename (str): pdf file name, can include absolute or relative path to file.
    """
    os.system("pdfcrop {0} {0}".format(filename))

def draw_horizontal_arrow(ax, x, y, length, width=0.001, color="#FF9E00", alpha=1, lw=1, head_width=0.1, head_length=0.1, overhang=0.1, **kwargs):
    """_summary_

    Args:
        ax (_type_): _description_
        x (_type_): _description_
        y (_type_): _description_
        length (_type_): _description_
        width (_type_): _description_
        color (str, optional): _description_. Defaults to "black".
        alpha (int, optional): _description_. Defaults to 1.
        lw (int, optional): _description_. Defaults to 1.
        head_width (float, optional): _description_. Defaults to 0.1.
        head_length (float, optional): _description_. Defaults to 0.1.
        overhang (float, optional): _description_. Defaults to 0.1.
    """
    ax.arrow(x, y, dx=length, dy=0, width=width, color=color, alpha=alpha, lw=lw, head_width=head_width, head_length=head_length, overhang=overhang, **kwargs)

def get_ratio(ratio: str) -> float:
    a, b = [float(v) for v in ratio.split(":")]
    return b/a

def set_sub_fig_ref(ax, index, sub_index=None, pos=np.zeros(2), **kwargs):
    """_summary_
    Postion letter for for sub figure according the given index et the pos
    Args:
        ax (_type_): mpl axes object
        index (int): index of figure
        pos (ndarray): x,y position of reference text
    """
    if sub_index is None:
        ax.text(
                pos[0],
                pos[1],
                ascii_lowercase[index],
                **kwargs
            )
    else:
        ax.text(
                pos[0],
                pos[1],
                f"{ascii_lowercase[index]}-{sub_index}",
                **kwargs
            )
        
def harmonies_figures(frame_ref, frame):
    h0, w0 ,c0 = frame_ref.shape 
    h1, w1 ,c1 = frame.shape

    frame_out = np.zeros((h0, w1, c1), dtype=np.uint8)
    v = int((h0-h1)/2)
    frame_out[v:v+h1,:,:] = frame
    
    return frame_out

def homography(frame, c0, c1, c2 ,c3):
    pts_src = np.array([c0,c1,c2,c3])
    pts_dst = np.array([[0,                          0],
                        [frame.shape[1],             0],
                        [frame.shape[1],frame.shape[0]],
                        [0,             frame.shape[0]]])
    h, status = cv2.findHomography(pts_src, pts_dst)
    size = (frame.shape[1], frame.shape[0])
    frame = cv2.warpPerspective(frame, h, size)
    return frame

def sub_axins(
    ax,
    frames,
    ind,
    axins_corners=np.zeros(4),
    ax_corners=np.zeros(4),
    c0_out=np.zeros(2),
    c1_out=np.zeros(2),
    edges="--w"
):
    """_summary_
    Only for images sub_axins
    Args:
        ax (_type_): _description_
        frames (_type_): _description_
        ind (_type_): _description_
        axins_corners (_type_, optional): _description_. Defaults to np.zeros(4).
        ax_corners (_type_, optional): _description_. Defaults to np.zeros(4).
        c0_out (_type_, optional): _description_. Defaults to np.zeros(2).
        c1_out (_type_, optional): _description_. Defaults to np.zeros(2).
    """
    def _corners_selection(c, x, y):
        if len(c.shape) <= 1:
            cc = np.array([[x, y], c])
        else:
            cc = np.array([c[0], c[1]])

        return cc

    axins = ax.inset_axes(axins_corners, transform=ax.transAxes, facecolor="lightgray")
    axins.imshow(frames[ind], interpolation="nearest", origin="upper")
    x1, x2, y1, y2 = ax_corners
    axins.set_xlim(x1, x2)
    axins.set_ylim(y1, y2)
    for axis in ["top", "bottom", "left", "right"]:
        axins.spines[axis].set_linewidth(2)
        axins.spines[axis].set_color("r")
    axins.set_xticks([])
    axins.set_yticks([])
    # axins.grid()

    pp, p1, p2 = mark_inset(
        parent_axes=ax,
        inset_axes=axins,
        loc1=2,
        loc2=3,
        edgecolor="None",
        linewidth=1,
    )
    pp.set_fill(False)
    pp.set_facecolor("None")
    pp.set_edgecolor("r")
    pp.set_visible(True)
    # axins.axis("off")

    c0 = _corners_selection(c0_out, x1, y2)
    c1 = _corners_selection(c1_out, x2, y1)

    ax.plot(c0[:, 0], c0[:, 1], edges, linewidth=1)
    ax.plot(c1[:, 0], c1[:, 1], edges, linewidth=1)

def img_convert(filename, out_filename, *arg, **kwargs):
    
    # if not 'compression' in kwargs:
    #      kwargs["compression"]="none"
        
    with Image.open(filename) as im:
        im.save(out_filename, *arg, **kwargs)

def get_figure_default_setup(ratio="4:3", width_cm=21, orientation="portrait"):
    """_summary_

    Args:
        ratio (str, optional): 4:3, 16:9, 16:10. Defaults to "4:3".
        width_cm (int, optional): _description_. Defaults to 21 (A4).
        orientation (str, optional): portrait or landscape. Defaults to "portrait".

    Returns:
        tuple: (width, height). Figure size in inches
    """
    cm2inches = .3937
    figwidth = width_cm
    if orientation == "portrait":
        figheight = figwidth/get_ratio(ratio)
    else:
        figheight = figwidth*get_ratio(ratio)
    return figwidth*cm2inches, figheight*cm2inches


def get_bbox_props(pad_style="round", pad_size=0.1, fc="white", ec="black", lw=0.2,alpha=0):
    """_summary_

    Args:
        pad_style (str, optional): _description_. Defaults to "round".
        pad_size (float, optional): _description_. Defaults to 0.1.
        fc (str, optional): _description_. Defaults to "white".
        ec (str, optional): _description_. Defaults to "black".
        lw (float, optional): _description_. Defaults to 0.2.

    Returns:
        dict : bbox_props
    """
    return dict(boxstyle=f'{pad_style},pad={str(pad_size)}', alpha=alpha, fc=fc, ec=ec, lw=lw)


def check_fig_postion(ax, step=21, camera=False):
    """_summary_

    Args:
        ax (_type_): _description_
        xmax (_type_): _description_
        ymax (_type_): _description_
        xmin (int, optional): _description_. Defaults to 0.
        ymin (int, optional): _description_. Defaults to 0.
        step (int, optional): _description_. Defaults to 20.
        camera (bool, optional): _description_. Defaults to False.
    """
    xmax = ax.get_xlim()[1]
    xmin = ax.get_xlim()[0]
    ymax = ax.get_ylim()[1]
    ymin = ax.get_ylim()[0]
    ax.axis('on')
    ax.set_xticks(np.linspace(start=xmin, stop=xmax, num=step))
    ax.set_yticks(np.linspace(start=ymin, stop=ymax, num=step))
    ax.set_xticklabels([round(v, 2)
                        for v in np.linspace(start=0, stop=1, num=step)])
    ax.set_yticklabels([round(v, 2)
                        for v in np.linspace(start=0, stop=1, num=step)])
    if camera:
        ax.set_yticklabels([round(v, 2)
                            for v in np.linspace(start=1, stop=0, num=step)])

    ax.grid(which='major', axis='both', linestyle='-')


def resize_img(img, width, height):
    """_summary_

    Args:
        img (_type_): _description_
        width (_type_): _description_
        height (_type_): _description_

    Returns:
        _type_: _description_
    """
    dim = (width, height)
    return cv2.resize(img, dim, interpolation=cv2.INTER_AREA)


def cropper(start_point, end_point, img):
    """_summary_

    Args:
        start_point (_type_): _description_
        end_point (_type_): _description_
        img (_type_): _description_

    Returns:
        _type_: _description_
    """
    return img[start_point[1]: end_point[1], start_point[0]: end_point[0], :]


def increase_brightness(img, value=60):
    """_summary_

    Args:
        img (_type_): _description_
        value (int, optional): _description_. Defaults to 60.

    Returns:
        _type_: _description_
    """
    hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
    h, s, v = cv2.split(hsv)

    lim = 255 - value
    v[v > lim] = 255
    v[v <= lim] += value

    final_hsv = cv2.merge((h, s, v))
    img = cv2.cvtColor(final_hsv, cv2.COLOR_HSV2RGB)
    return img


def add_arrow(line, position=None, direction='right', size=15, color=None, arrowstyle='->', *args, **kwargs):
    """
    Source : https://stackoverflow.com/questions/34017866/arrow-on-a-line-plot-with-matplotlibs
    add an arrow to a line.

    line:       Line2D object
    position:   x-position of the arrow. If None, mean of xdata is taken
    direction:  'left' or 'right'
    size:       size of the arrow in fontsize points
    color:      if None, line color is taken.
    """
    if color is None:
        color = line.get_color()

    xdata = line.get_xdata()
    ydata = line.get_ydata()

    if position is None:
        position = xdata.mean()
    # find closest index
    start_ind = np.argmin(np.absolute(xdata - position))
    end_ind = start_ind + 1 if direction == 'right' else start_ind - 1
    line.axes.annotate('',
        xytext=(xdata[start_ind], ydata[start_ind]),
        xy=(xdata[end_ind], ydata[end_ind]),
        arrowprops=dict(arrowstyle=arrowstyle, color=color, **kwargs),
        size=size
    )

from scipy import misc
import scipy.ndimage


def gaussian_blur(sharp_image, sigma):
    # https://stackoverflow.com/questions/54826757/what-is-the-most-elegant-way-to-blur-parts-of-an-image-using-python
    # Filter channels individually to avoid gray scale images
    blurred_image_r = scipy.ndimage.filters.gaussian_filter(sharp_image[:, :, 0], sigma=sigma)
    blurred_image_g = scipy.ndimage.filters.gaussian_filter(sharp_image[:, :, 1], sigma=sigma)
    blurred_image_b = scipy.ndimage.filters.gaussian_filter(sharp_image[:, :, 2], sigma=sigma)
    blurred_image = np.dstack((blurred_image_r, blurred_image_g, blurred_image_b))
    return blurred_image


def uniform_blur(sharp_image, uniform_filter_size):
    # https://stackoverflow.com/questions/54826757/what-is-the-most-elegant-way-to-blur-parts-of-an-image-using-python
    # The multidimensional filter is required to avoid gray scale images
    multidim_filter_size = (uniform_filter_size, uniform_filter_size, 1)
    blurred_image = scipy.ndimage.filters.uniform_filter(sharp_image, size=multidim_filter_size)
    return blurred_image


def blur_image_locally(sharp_image, mask, use_gaussian_blur, gaussian_sigma, uniform_filter_size):
    # https://stackoverflow.com/questions/54826757/what-is-the-most-elegant-way-to-blur-parts-of-an-image-using-python
    
    # Debug 
    one_values_f32 = np.full(sharp_image.shape, fill_value=1.0, dtype=np.float32)[:,:,:3]
    sharp_image_f32 = sharp_image.astype(dtype=np.float32)
    sharp_mask_f32 = mask.astype(dtype=np.float32)

    if use_gaussian_blur:
        blurred_image_f32 = gaussian_blur(sharp_image_f32, sigma=gaussian_sigma)
        blurred_mask_f32 = gaussian_blur(sharp_mask_f32, sigma=gaussian_sigma)
        
    else:
        blurred_image_f32 = uniform_blur(sharp_image_f32, uniform_filter_size)
        blurred_mask_f32 = uniform_blur(sharp_mask_f32, uniform_filter_size)

    blurred_mask_inverted_f32 = one_values_f32 - blurred_mask_f32
    weighted_sharp_image = np.multiply(sharp_image_f32, blurred_mask_f32)
    weighted_blurred_image = np.multiply(blurred_image_f32, blurred_mask_inverted_f32)
    locally_blurred_image_f32 = weighted_sharp_image + weighted_blurred_image

    locally_blurred_image = locally_blurred_image_f32.astype(dtype=np.uint8)

    return locally_blurred_image

def get_blur_mask(interest_loc, ref_frame):
    height, width, channels = ref_frame.shape
    mask = np.full((height, width, channels), fill_value=0)
    mask[interest_loc[0,1]:interest_loc[1,1],interest_loc[0,0]:interest_loc[1,0],:] = 1
    return mask

class AngleAnnotation(Arc):
    """
    Draws an arc between two vectors which appears circular in display space.
    """

    def __init__(self, xy, p1, p2, size=75, unit="points", ax=None,
                 text="", textposition="inside", text_kw=None, **kwargs):
        """
        Parameters
        ----------
        xy, p1, p2 : tuple or array of two floats
            Center position and two points. Angle annotation is drawn between
            the two vectors connecting *p1* and *p2* with *xy*, respectively.
            Units are data coordinates.

        size : float
            Diameter of the angle annotation in units specified by *unit*.

        unit : str
            One of the following strings to specify the unit of *size*:

            * "pixels": pixels
            * "points": points, use points instead of pixels to not have a
              dependence on the DPI
            * "axes width", "axes height": relative units of Axes width, height
            * "axes min", "axes max": minimum or maximum of relative Axes
              width, height

        ax : `matplotlib.axes.Axes`
            The Axes to add the angle annotation to.

        text : str
            The text to mark the angle with.

        textposition : {"inside", "outside", "edge"}
            Whether to show the text in- or outside the arc. "edge" can be used
            for custom positions anchored at the arc's edge.

        text_kw : dict
            Dictionary of arguments passed to the Annotation.

        **kwargs
            Further parameters are passed to `matplotlib.patches.Arc`. Use this
            to specify, color, linewidth etc. of the arc.

        """
        self.ax = ax or plt.gca()
        self._xydata = xy  # in data coordinates
        self.vec1 = p1
        self.vec2 = p2
        self.size = size
        self.unit = unit
        self.textposition = textposition

        super().__init__(self._xydata, size, size, angle=0.0,
                         theta1=self.theta1, theta2=self.theta2, **kwargs)

        self.set_transform(IdentityTransform())
        self.ax.add_patch(self)

        self.kw = dict(ha="center", va="center",
                       xycoords=IdentityTransform(),
                       xytext=(0, 0), textcoords="offset points",
                       annotation_clip=True)
        self.kw.update(text_kw or {})
        self.text = ax.annotate(text, xy=self._center, **self.kw)

    def get_size(self):
        factor = 1.
        if self.unit == "points":
            factor = self.ax.figure.dpi / 72.
        elif self.unit[:4] == "axes":
            b = TransformedBbox(Bbox.from_bounds(0, 0, 1, 1),
                                self.ax.transAxes)
            dic = {"max": max(b.width, b.height),
                   "min": min(b.width, b.height),
                   "width": b.width, "height": b.height}
            factor = dic[self.unit[5:]]
        return self.size * factor

    def set_size(self, size):
        self.size = size

    def get_center_in_pixels(self):
        """return center in pixels"""
        return self.ax.transData.transform(self._xydata)

    def set_center(self, xy):
        """set center in data coordinates"""
        self._xydata = xy

    def get_theta(self, vec):
        vec_in_pixels = self.ax.transData.transform(vec) - self._center
        return np.rad2deg(np.arctan2(vec_in_pixels[1], vec_in_pixels[0]))

    def get_theta1(self):
        return self.get_theta(self.vec1)

    def get_theta2(self):
        return self.get_theta(self.vec2)

    def set_theta(self, angle):
        pass

    # Redefine attributes of the Arc to always give values in pixel space
    _center = property(get_center_in_pixels, set_center)
    theta1 = property(get_theta1, set_theta)
    theta2 = property(get_theta2, set_theta)
    width = property(get_size, set_size)
    height = property(get_size, set_size)

    # The following two methods are needed to update the text position.
    def draw(self, renderer):
        self.update_text()
        super().draw(renderer)

    def update_text(self):
        c = self._center
        s = self.get_size()
        angle_span = (self.theta2 - self.theta1) % 360
        angle = np.deg2rad(self.theta1 + angle_span / 2)
        r = s / 2
        if self.textposition == "inside":
            r = s / np.interp(angle_span, [60, 90, 135, 180],
                              [3.3, 3.5, 3.8, 4])
        self.text.xy = c + r * np.array([np.cos(angle), np.sin(angle)])
        if self.textposition == "outside":
            def R90(a, r, w, h):
                if a < np.arctan(h/2/(r+w/2)):
                    return np.sqrt((r+w/2)**2 + (np.tan(a)*(r+w/2))**2)
                else:
                    c = np.sqrt((w/2)**2+(h/2)**2)
                    T = np.arcsin(c * np.cos(np.pi/2 - a + np.arcsin(h/2/c))/r)
                    xy = r * np.array([np.cos(a + T), np.sin(a + T)])
                    xy += np.array([w/2, h/2])
                    return np.sqrt(np.sum(xy**2))

            def R(a, r, w, h):
                aa = (a % (np.pi/4))*((a % (np.pi/2)) <= np.pi/4) + \
                     (np.pi/4 - (a % (np.pi/4)))*((a % (np.pi/2)) >= np.pi/4)
                return R90(aa, r, *[w, h][::int(np.sign(np.cos(2*a)))])

            bbox = self.text.get_window_extent()
            X = R(angle, r, bbox.width, bbox.height)
            trans = self.ax.figure.dpi_scale_trans.inverted()
            offs = trans.transform(((X-s/2), 0))[0] * 72
            self.text.set_position([offs*np.cos(angle), offs*np.sin(angle)])
