from setuptools import setup

setup(
    name="plot_toolbox",
    version="0.0.0",
    description="Python_toolbox for plotting",
    long_description="Python_toolbox for plotting",
    author="Ludovic Charleux, Christian Elmo",
    author_email="ludovic.charleux@univ-smb.fr",
    license="GPL v3",
    packages=["plot_toolbox"],
    url="https://gitlab.com/symmehub/utils/plot_toolbox",
)
